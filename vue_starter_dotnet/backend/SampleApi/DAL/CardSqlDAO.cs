﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using SampleApi.Models;

namespace SampleApi.DAL
{
    public class CardSqlDAO : ICardDAO
    {

        private readonly string connectionString;

        public CardSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<string> GetAllSetCodes()
        {
            List<string> result = new List<string>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select setCode, setName from cards group by setCode, setName order by setName asc; ", conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string set = Convert.ToString(reader["setCode"]);
                        result.Add(set);
                        
                    }

                }
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        public List<string> GetAllSetNames()
        {
            List<string> result = new List<string>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select setCode, setName from cards group by setCode, setName order by setName asc;", conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        string set = Convert.ToString(reader["setName"]);
                        result.Add(set);

                    }

                }
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        public Card GetCardByID(string id)
        {
            Card result = new Card();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Select * FROM cards WHERE cardId = @cardId;", conn);
                    cmd.Parameters.AddWithValue("@cardId", id);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        Card card = ConvertReaderToCard(reader);
                        result = card;
                        return result;
                    }

                }
            }
            catch(Exception ex)
            {
                
                
            }

            return result;
        }

        public List<Card> GetCardList()
        {
            List<Card> result = new List<Card>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM cards", conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while(reader.Read())
                    {
                        Card card = ConvertReaderToCard(reader);
                        result.Add(card);
                    }
                }
            }
            catch(Exception ex)
            {

            }

            return result;
        }

        public List<Card> GetCardListByName(string name)
        {
            List<Card> result = new List<Card>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM cards WHERE name like @name", conn);
                    cmd.Parameters.AddWithValue("@name", "%" + name + "%");
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Card card = ConvertReaderToCard(reader);
                        result.Add(card);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public List<Card> GetCardListByRarity(string rarity)
        {
            List<Card> result = new List<Card>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM cards WHERE rarity = @rarity", conn);
                    cmd.Parameters.AddWithValue("@rarity", rarity);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Card card = ConvertReaderToCard(reader);
                        result.Add(card);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public List<Card> GetCardListBySetName(string setName)
        {
            List<Card> result = new List<Card>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM cards WHERE setName = @setName order by TRY_PARSE(cardNumber as int), cardNumber asc", conn);
                    cmd.Parameters.AddWithValue("@setName", setName);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Card card = ConvertReaderToCard(reader);
                        result.Add(card);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public string GetSetNameFromSetCode(string setCode)
        {
            string result = "";
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT setName FROM cards WHERE setCode = @setCode GROUP BY setName", conn);
                    cmd.Parameters.AddWithValue("@setCode", setCode);
                    result = Convert.ToString(cmd.ExecuteScalar());
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public List<Card> GetCardListBySuperType(string superType)
        {
            List<Card> result = new List<Card>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM cards WHERE superType = @superType", conn);
                    cmd.Parameters.AddWithValue("@superType", superType);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Card card = ConvertReaderToCard(reader);
                        result.Add(card);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public List<Card> GetCardListByType(string type)
        {
            List<Card> result = new List<Card>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM cards WHERE type = @type", conn);
                    cmd.Parameters.AddWithValue("@type", type);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Card card = ConvertReaderToCard(reader);
                        result.Add(card);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        private Card ConvertReaderToCard(SqlDataReader reader)
        {
            Card card = new Card();
            card.CardID = Convert.ToString(reader["cardID"]);
            card.CardName = Convert.ToString(reader["name"]);
            card.ImageURL = Convert.ToString(reader["imageURL"]);
            card.ImageURLHiRes = Convert.ToString(reader["imageURLHighRes"]);           
            card.Type = Convert.ToString(reader["type"]);
            if (card.Type == null)
            {
                card.Type = card.SuperType;
            }
            card.SuperType = Convert.ToString(reader["superType"]);
            card.HP = Convert.ToString(reader["hP"]);
            if (card.HP == null)
            {
                card.HP = "0";
            }
            card.CardNumber = Convert.ToString(reader["cardNumber"]);
            card.Rarity = Convert.ToString(reader["rarity"]);
            card.Series = Convert.ToString(reader["series"]);
            card.SetName = Convert.ToString(reader["setName"]);
            card.SetCode = Convert.ToString(reader["setCode"]);

            return card;
        }
    }
}
