﻿using SampleApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleApi.DAL
{
   public interface ICardDAO
    {
        List<Card> GetCardList();

        List<Card> GetCardListByType(string type);

        List<Card> GetCardListByName(string name);

        List<Card> GetCardListBySuperType(string superType);

        List<Card> GetCardListByRarity(string rarity);

        List<Card> GetCardListBySetName(string setName);

        Card GetCardByID(string id);

        List<String> GetAllSetNames();

        List<String> GetAllSetCodes();

        string GetSetNameFromSetCode(string setCode);


    }
}
