﻿using SampleApi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SampleApi.DAL
{
    public class CollectionSqlDAO : ICollectionDAO

    {
        private readonly string connectionString;

        public CollectionSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public int CreateCollection(bool isPublic, string username, string collectionName)
        {
            int collectionID = -1;

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO collections VALUES ((Select id from users WHERE username = @username), @isPublic, @collectionName); select scope_identity()", conn);
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@isPublic", isPublic);
                    cmd.Parameters.AddWithValue("@collectionName", collectionName);

                    collectionID = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }catch(Exception ex)
            {
                
            }

            return collectionID;
        }

        public bool AddCardToCollection(CollectionCardModel model)
        {
            bool isWorking = true;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Insert INTO collection_card VALUES (@collectionID, @cardID);", conn);
                    cmd.Parameters.AddWithValue("@collectionID", model.CollectionID);
                    cmd.Parameters.AddWithValue("@cardID", model.CardID);

                    cmd.ExecuteScalar();
                    
                }
            }catch(Exception ex)
            {
                isWorking = false;
               
            }
            return isWorking;
        }

        public string GetCollectionNameFromID(int collectionID)
        {
            string result = "";
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Select collectionName FROM collections WHERE collection_id = @collectionID;", conn);
                    cmd.Parameters.AddWithValue("@collectionID", collectionID);
                    result = Convert.ToString(cmd.ExecuteScalar());

                }
            }
            catch(Exception ex)
            {
                
            }
            return result;
        }

        public List<Collection> GetCollectionByUser(string userName)
        {
            List<Collection> collectionList = new List<Collection>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Select * FROM 'collections' WHERE  user_ID = (Select id from users where username = @username) AND isPublic = 1;", conn);
                    cmd.Parameters.AddWithValue("@username", userName);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Collection collection = ConvertReaderToCollection(reader);
                        collectionList.Add(collection);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return collectionList;
        }

        public bool ToggleCollectionPrivacy(int collectionID)
        {
            bool isWorking = true;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("UPDATE collections Set isPublic = (1 - isPublic) WHERE collection_id = @collectionID;", conn);
                    cmd.Parameters.AddWithValue("@collectionID", collectionID);
                }
            }catch(Exception ex)
            {
                isWorking = false;
            }
            return isWorking;
        }

        public List<Card> GetCardsInCollection(int collectionID)
        {
            List<Card> result = new List<Card>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * from cards JOIN collection_card on collection_card.card_id = cards.cardID WHERE collection_card.collection_id = @collectionID", conn);
                    cmd.Parameters.AddWithValue("@collectionID", collectionID);
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Card card = ConvertReaderToCard(reader);
                        result.Add(card);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public int InsertCardIntoCollection(int collectionID, string cardID)
        {
            int result = -1;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO TABLE collection_card VALUES (@collection_id, @card_id; Select scope_identity()", conn);
                    cmd.Parameters.AddWithValue("@collection_id", collectionID);
                    cmd.Parameters.AddWithValue("@card_id", cardID);
                    result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result;
                }
            }
            catch(Exception ex)
            {
                return result;
            }
        }

        public int GetCountOfCardsInCollection(int collectionID)
        {
            int result = -1;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Select count(card_id) from collection_card where collection_id = @collection_id; select scope_identity()", conn);
                    cmd.Parameters.AddWithValue("@collection_id", collectionID);
                    result = Convert.ToInt32(cmd.ExecuteScalar());
                    
                }
            }
            catch(Exception ex)
            {

            }
            return result;
        }

        public int GetCountOfCardsInCollectionByType(int collectionID, string type)
        {
            int results = -1;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Select Count(card_id) from collection_card JOIN cards on collection_card.card_id = cards.cardId where collection_id = @collection_id AND type = @type; select scope_identity()", conn);
                    cmd.Parameters.AddWithValue("@collection_id", collectionID);
                    cmd.Parameters.AddWithValue("@type", type);
                    results = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }
            catch(Exception ex)
            {

            }
            return results;
        }

        public int GetCountOfCardsInAllCollections()
        {
            int results = -1;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Select Count(card_id) from collection_card; select scope_identity()", conn);
                    results = Convert.ToInt32(cmd.ExecuteScalar());

                }
            }catch(Exception ex)
            {

            }
            return results;
        }

        public int GetCountOfCardsInAllCollectionsByType(string type)
        {
            int results = -1;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Select Count(card_id) from collection_card Join cards ON collection_card.card_id = cards.cardId WHERE type = @type; select scope_identity()", conn);
                    cmd.Parameters.AddWithValue("@type", type);
                    results = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }catch(Exception ex)
            {

            }
            return results;
        }

        private Card ConvertReaderToCard(SqlDataReader reader)
        {
            Card card = new Card();
            card.CardID = Convert.ToString(reader["cardID"]);
            card.CardName = Convert.ToString(reader["name"]);
            card.ImageURL = Convert.ToString(reader["imageURL"]);
            card.ImageURLHiRes = Convert.ToString(reader["imageURLHighRes"]);
            card.Type = Convert.ToString(reader["type"]);
            card.SuperType = Convert.ToString(reader["superType"]);
            card.HP = Convert.ToString(reader["hP"]);
            card.CardNumber = Convert.ToString(reader["cardNumber"]);
            card.Rarity = Convert.ToString(reader["rarity"]);
            card.Series = Convert.ToString(reader["series"]);
            card.SetName = Convert.ToString(reader["setName"]);
            card.SetCode = Convert.ToString(reader["setCode"]);

            return card;
        }

        private Collection ConvertReaderToCollection(SqlDataReader reader)
        {
            Collection collection = new Collection();
            collection.CollectionID = Convert.ToInt32(reader["collection_id"]);
            collection.UserID = Convert.ToInt32(reader["user_id"]);
            collection.IsPublic = Convert.ToBoolean(reader["isPublic"]);
            collection.CollectionName = Convert.ToString(reader["collectionName"]);

            return collection;
        }
    }
}
