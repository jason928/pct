﻿using SampleApi.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace SampleApi.DAL
{
    public interface ICollectionDAO
    {
        int CreateCollection(bool isPublic, string username, string collectionName);

        string GetCollectionNameFromID(int collectionID);

        List<Collection> GetCollectionByUser(string userName);

        bool AddCardToCollection(CollectionCardModel model);

        bool ToggleCollectionPrivacy(int collectionID);

        List<Card> GetCardsInCollection(int collectionID);

        int InsertCardIntoCollection(int collectionID, string cardID);

        int GetCountOfCardsInCollection(int collectionID);

        int GetCountOfCardsInAllCollections();

        int GetCountOfCardsInAllCollectionsByType(string type);

        int GetCountOfCardsInCollectionByType(int collectionID, string type);
    }
}
