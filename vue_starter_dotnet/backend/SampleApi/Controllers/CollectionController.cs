﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SampleApi.DAL;
using SampleApi.Models;

namespace SampleApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class CollectionController : ControllerBase
    {
        private IUserDAO userDAO;
        private ICollectionDAO collectionDAO;
        public CollectionController(IUserDAO userDAO, ICollectionDAO collectionDAO)
        {
            this.userDAO = userDAO;
            this.collectionDAO = collectionDAO;
        }

        [HttpPost("newcollection")]
        public IActionResult NewCollection(NewCollectionModel collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            int collectionID = collectionDAO.CreateCollection(collection.IsPublic, collection.UserName, collection.CollectionName);
            return Ok(collectionID);
        }

        [HttpPost("addcard")]
        public IActionResult AddCardToCollection(CollectionCardModel model)
        {
            collectionDAO.AddCardToCollection(model);
            return Ok();
        }

        [HttpPost("getname")]
        public IActionResult GetCollectionNameFromId(Collection collection)
        {
            string result = collectionDAO.GetCollectionNameFromID(collection.CollectionID);
            return Ok(result);
        }

        [HttpPost("getcards")]
        public IActionResult GetCardsInCollection(Collection collection)
        {
            List<Card> result = collectionDAO.GetCardsInCollection(collection.CollectionID);
            return Ok(result);
        }

        [HttpPost("getcountofcardsinallcollections")]
        public IActionResult GetCountOfCardsInAllCollections()
        {
            int result = collectionDAO.GetCountOfCardsInAllCollections();
            return Ok(result);
        }

        [HttpPost("getcountofcardsinallcollectionsbytype")]
        public IActionResult GetCountOfCardsInAllCollectionsByType(Card card)
        {
            int result = collectionDAO.GetCountOfCardsInAllCollectionsByType(card.Type);
            return Ok(result);
        }

        [HttpPost("getcountofcardsincollection")]
        public IActionResult GetCountOfCardsInCollection(Collection collection)
        {
            int result = collectionDAO.GetCountOfCardsInCollection(collection.CollectionID);
            return Ok(result);
        }

        [HttpPost("getcountofcardsincollectionbytype")]
        public IActionResult GetCountOfCardsInCollectionByType(CollectionCardModel model)
        {
            int result = collectionDAO.GetCountOfCardsInCollectionByType(model.CollectionID, model.Type);
            return Ok(result);
        }


    }
}