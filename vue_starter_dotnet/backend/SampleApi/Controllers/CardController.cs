﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SampleApi.DAL;
using SampleApi.Models;

namespace SampleApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class CardController : ControllerBase
    {
        private IUserDAO userDAO;
        private ICollectionDAO collectionDAO;
        private ICardDAO cardDAO;
        public CardController(IUserDAO userDAO, ICollectionDAO collectionDAO, ICardDAO cardDAO)
        {
            this.userDAO = userDAO;
            this.collectionDAO = collectionDAO;
            this.cardDAO = cardDAO;
        }

        [HttpPost("getcardbyid")]
        public IActionResult GetCardByID( string cardID)
        {
            cardDAO.GetCardByID(cardID);
            return Ok();
        }

        [HttpPost("getcardlist")]
        public IActionResult GetCardList()
        {
            cardDAO.GetCardList();
            return Ok();
        }

        [HttpPost("getcardlistbyname")]
        public IActionResult GetCardListByName(string name)
        {
            cardDAO.GetCardListByName(name);
            return Ok();
        }

        [HttpPost("getcardlistbyrarity")]
        public IActionResult GetCardListByRarity(string rarity)
        {
            cardDAO.GetCardListByRarity(rarity);
            return Ok();
        }

        [HttpPost("getcardlistbysetname")]
        public IActionResult GetCardListBySetName(string setName)
        {
            List<Card> result = cardDAO.GetCardListBySetName(setName);
            return Ok(result);
        }

        [HttpPost("getcardlistbysetcode")]
        public IActionResult GetCardListBySetCode(Card card)
        {
            List<Card> result = cardDAO.GetCardListBySetName(cardDAO.GetSetNameFromSetCode(card.SetCode));
            return Ok(result);
        }

        [HttpPost("getcardlistbysupertype")]
        public IActionResult GetCardListBySuperType(string superType)
        {
            cardDAO.GetCardListBySuperType(superType);
            return Ok();
        }

        [HttpPost("getcardlistbytype")]
        public IActionResult GetCardListByType(string type)
        {
            cardDAO.GetCardListByType(type);
            return Ok();
        }

        [HttpPost("getsetnamelist")]
        public IActionResult GetSetNameList()
        {
            List<string> result = cardDAO.GetAllSetNames();
            return Ok(result);
        }

        [HttpPost("getsetcodelist")]
        public IActionResult GetSetCodeList()
        {
            List<string> result = cardDAO.GetAllSetCodes();
            return Ok(result);
        }

        [HttpPost("getsetnamefromsetcode")]
        public IActionResult GetSetNameFromSetCode(Card card)
        {
            string result = cardDAO.GetSetNameFromSetCode(card.SetCode);
            return Ok(result);
        }
    }
}