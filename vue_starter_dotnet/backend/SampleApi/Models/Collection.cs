﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleApi.Models
{
    public class Collection
    {
        public int CollectionID { get; set; }

        public int UserID { get; set; }

        public bool IsPublic { get; set; }

        public string CollectionName { get; set; }
    }
}
