﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleApi.Models
{
    public class NewCollectionModel
    {
        public int CollectionID { get; set; }

        public string UserName { get; set; }

        public bool IsPublic { get; set; }

        public string CollectionName { get; set; }
    }
}
