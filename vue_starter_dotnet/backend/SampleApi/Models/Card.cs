﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleApi.Models
{
    public class Card
    {
        public string CardID { get; set; }

        public string CardName { get; set; }

        public string ImageURL { get; set; }

        public string ImageURLHiRes { get; set; }

        public string Type { get; set; }

        public string SuperType { get; set; }

        public string HP { get; set; }

        public string CardNumber { get; set; } //some of the numbers are alphanumeric, so it's a string

        public string Rarity { get; set; }

        public string Series { get; set; }

        public string SetName { get; set; }

        public string SetCode { get; set; }
    }
}
