﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Pokemon_Database_Access
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get the connection string from the appsettings.json file
            //IConfigurationBuilder builder = new ConfigurationBuilder()
            //    .SetBasePath(Directory.GetCurrentDirectory())
            //    .AddJsonFile("jsconfig1.json", optional: true, reloadOnChange: true);

            //IConfigurationRoot configuration = builder.Build();

            string connectionString = "Server=.\\SQLEXPRESS;Database=DemoDB;Trusted_Connection=True;";
            pokemonDAO pokeDAO = new pokemonDAO(connectionString);
            pokeDAO.InsertPokemonCards();
            energyDAO eDAO = new energyDAO(connectionString);
            trainerDAO tDAO = new trainerDAO(connectionString);
            eDAO.InsertPokemonCards();
            tDAO.InsertPokemonCards();

        }
    }
}
