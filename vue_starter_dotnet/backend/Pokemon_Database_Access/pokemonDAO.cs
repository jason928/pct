﻿using PokemonTcgSdk;
using PokemonTcgSdk.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Pokemon_Database_Access
{
    class pokemonDAO
    {
        private string connectionString { get; }
        public pokemonDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public void InsertPokemonCards()
        {
            List<PokemonCard> cards = Card.All();
            foreach (PokemonCard card in cards)
            {
                if (card.SuperType.ToLower() == "pokémon")
                {
                    string working = insertPokemon(card.Id, card.Name, card.ImageUrl, card.ImageUrlHiRes,card.Types[0], card.SuperType, card.Hp, card.Number, card.Rarity, card.Series, card.Set, card.SetCode);
                    Console.WriteLine(card.Id)/* + " " + card.Name + " " + card.SetCode + " " + card.Set + " " + card.Series + " " + card.SuperType + " " + card.Types[0])*/;
                }
            }
            return;
        }

        

        public string insertPokemon(string iD, string name, string imageUrl, string highRes, string type, string superType, string hp, string cardNumber, string rarity, string series, string setName, string setCode)
        {
            string tableInt;
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("INSERT INTO cards Values (@cardId, @name, @imageURL, @imageURLHighRes, @type, @superType, @hP, @cardNumber, @rarity, @series, @setName, @setCode); select scope_identity();", connection);

                    cmd.Parameters.AddWithValue("@cardId", iD);
                    cmd.Parameters.AddWithValue("@name", name);
                    cmd.Parameters.AddWithValue("@imageURL", imageUrl);
                    cmd.Parameters.AddWithValue("@imageURLHighRes", highRes);
                    cmd.Parameters.AddWithValue("@type", type);
                    cmd.Parameters.AddWithValue("@superType", superType);
                    cmd.Parameters.AddWithValue("@hP", hp);
                    cmd.Parameters.AddWithValue("@cardNumber", cardNumber);
                    cmd.Parameters.AddWithValue("@rarity", rarity);
                    cmd.Parameters.AddWithValue("@series", series);
                    cmd.Parameters.AddWithValue("@setName", setName);
                    cmd.Parameters.AddWithValue("@setCode", setCode);

                    tableInt = Convert.ToString(cmd.ExecuteScalar());

                    return tableInt;

                }
            }
            catch (Exception ex)
            {
                string didNotWork = "did not work";
                return didNotWork;
            }
        }
    }
}
