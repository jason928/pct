import Vue from 'vue'
import App from './App.vue'
import router from './router'

//persistent state code
import persistentState from 'vue-persistent-state'

let initialState = { 
  persisted: {
  loggedIn: false,
  currentUser: '',
  activeCollection: 0,
  }
}

Vue.use(persistentState, initialState);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
