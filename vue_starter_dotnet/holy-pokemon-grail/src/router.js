import Vue from 'vue'
import Router from 'vue-router'
import auth from './auth'
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import Register from './components/Register.vue'
import UserProfile from './components/UserProfile.vue'
import ViewCollection from './components/ViewCollection.vue'
import FontTest from './components/FontTest.vue'
import Stats from './components/Stats.vue'
import NewCollection from './components/NewCollection.vue'
import SelectCollection from './components/SelectCollection.vue'
import AllCollections from './components/AllCollections.vue'
import AddCardToCollection from './components/AddCardToCollection'
import SetList from './components/SetList'
import CardList from './components/CardList'

Vue.use(Router)

/**
 * The Vue Router is used to "direct" the browser to render a specific view component
 * inside of App.vue depending on the URL.
 *
 * It also is used to detect whether or not a route requires the user to have first authenticated.
 * If the user has not yet authenticated (and needs to) they are redirected to /login
 * If they have (or don't need to) they're allowed to go about their way.
 */

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior() {
    window.scrollTo(0,0);
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/register",
      name: "register",
      component: Register,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/user/:username",
      name: "user",
      component: UserProfile,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/collection/",
      name: "select-collection",
      component: SelectCollection,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/collection/:collectionID",
      name: "view-collection",
      component: ViewCollection,
      meta: {
        requiresAuth: false
      }
    },
    { 
      path: "/fonttest",
      name: "font-test",
      component: FontTest,
      meta: {
        requiresAuth: false
      }
    },
    { 
      path: "/stats",
      name: "stats",
      component: Stats,
      meta: {
        requiresAuth: false
      }
    },
    { 
      path: "/newcollection",
      name: "new-collection",
      component: NewCollection,
      meta: {
        requiresAuth: true
      }
    },
    { 
      path: "/allcollections",
      name: "all-collections",
      component: AllCollections,
      meta: {
        requiresAuth: false
      }
    },
    { 
      path: "/addcard",
      name: "add-card",
      component: AddCardToCollection,
      meta: {
        requiresAuth: true
      }
    },
    { 
      path: "/setlist",
      name: "set-list",
      component: SetList,
      meta: {
        requiresAuth: false
      }
    },
    { 
      path: "/cardlist/:setCode",
      name: "card-list",
      component: CardList,
      meta: {
        requiresAuth: false
      }
    },
  ]
})

router.beforeEach((to, from, next) => {
  // Determine if the route requires Authentication
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth);
  const user = auth.getUser();

  // If it does and they are not logged in, send the user to "/login"
  if (requiresAuth && !user) {
    next("/login");
  } else {
    // Else let them go to their next destination
    next();
  }
});

export default router;


